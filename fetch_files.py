import requests
from pathlib import Path

s = requests.Session()
s.headers.update({"User-Agent": "bjh21-fetch_files"})

with open("filelist") as fp:
    for line in fp:
        for fn in line.rstrip().split('|'):
            p = Path("files") / fn
            if not p.exists():
                r = s.get("https://commons.wikimedia.org/wiki/"
                          "Special:Redirect/file/" + fn)
                r.raise_for_status()
                p.write_bytes(r.content)
                print(f"fetched {fn}")
