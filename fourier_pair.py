import numpy as np
from numpy.fft import fft2, fftshift
import PIL
from matplotlib import pyplot
from argparse import ArgumentParser

parser = ArgumentParser()
parser.add_argument("image", nargs="+")
cfg = parser.parse_args()

fig, ax = pyplot.subplots(1, len(cfg.image) + 1)
ax0, *ax = ax

for i, imagefn in enumerate(cfg.image):
    image = PIL.Image.open(imagefn, formats=["TIFF", "PNG", "JPEG"])
    image = PIL.ImageOps.grayscale(image)
    image = np.array(image)

    ft = fftshift(fft2(image))

    if i == 0:
        ax0.set_axis_off()
        ax0.imshow(image, cmap="gray")
    ax[i].set_title(imagefn.replace("_", " "))
    ax[i].set_axis_off()
    ax[i].set_autoscale_on(True)
    ai = ax[i].imshow(abs(ft),
                      norm='log',
                      extent=(-ft.shape[1]/2, ft.shape[1]/2,
                              -ft.shape[0]/2, ft.shape[0]/2),
                      cmap="gray")
    #fig.colorbar(ai, ax=ax[i + 1])
    for j in range(len(cfg.image)):
        ax[j].set_autoscale_on(False)
        ax[j].axhline(-ft.shape[0]/2)
        ax[j].axhline(ft.shape[0]/2)
        ax[j].axvline(-ft.shape[1]/2)
        ax[j].axvline(ft.shape[1]/2)

pyplot.show()
