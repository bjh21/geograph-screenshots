import PIL.Image
import PIL.ImageDraw
from pathlib import Path

demosize = 200
gap = 20

# Co-ordinates are centre of the sample area
crops = {
    "Spondon, St Werburgh's Church north side GeoUK.jpg": (1444, 634),
    "Warburton St Werburgh from south-west GeoUK2370882.jpg": (968, 890),
    "Bainton St Andrews from NW 3954908.jpg": (1228, 922),
    "Ideford St Mary arcade GeoUK 3855313.jpg": (1498, 728),
    "Rowington St Laurence from WSW GeoUK 3396666.jpg": (1110, 428),
    "Wellesbourne St Peter eastward GeoUK 4793913.jpg": (1108, 710),
    "Thurlestone All Saints fr SSE + war-mem. GeoUK 3392293.jpg": (628, 812),
    "Doddington (Northum.) St Mary+Michael fr SW GeoUK 5288347.jpg": (426, 488),
    "Hughenden St Michael &AA chancel GeoUK 4312358.jpg": (262, 554),
    "Chalgrove St Mary eastward GeoUK 4075620.jpg": (1548, 968),
    "Ashbury St Mary Virg from N GeoUK 4126452.jpg": (1672, 378),
    "St Endellion nave+aisle looking NE GeoUK 4185921.jpg": (1076, 810),
    "Clyst St George parish church north face GeoUK4823519.jpg": (738, 1282),
    "Funtington chancel GeoUK 4168757.jpg": (1040, 954),
    "Funtington St Mary eastward GeoUK 4168777.jpg": (876, 906),
    "Sulllington St Mary eastward 2473483.jpg": (536, 1088),
    "Bainton St Andrew eastward GeoUK 5466038.jpg": (1110, 834),
}

ctr = 1

out_table = "{|class=wikitable\n!Geograph download||Screenshot||Demo\n"
out_list = ""
out_deltags = ""
out_gallery = "<gallery>\n"

with open("filelist") as fp:
    for line in fp:
        fn1, fn2 = line.rstrip().split('|')
        i1 = PIL.Image.open(Path("files") / fn1,
                            formats=["TIFF", "PNG", "JPEG"])
        i2 = PIL.Image.open(Path("files") / fn2,
                            formats=["TIFF", "PNG", "JPEG"])
        if max(*i2.size) > 1024 and i1.size > i2.size:
            fs1 = i1.size
            fs2 = i2.size
            out_table += ("|-\n"
                          f"|({fs2[0]} × {fs2[1]}) [[:File:{fn2}]]|"
                          f"|({fs1[0]} × {fs1[1]}) [[:File:{fn1}]]")
            out_list += f"* [[:File:{fn1}]]\n"
            out_deltags += f"File:{fn1}\n"
            out_deltags += ("{{delete|reason=[[COM:Redundant|]] with "
                            f"[[:File:{fn2}]]"
                            "|subpage=Screenshots of medium-sized "
                                     "Geograph images"
                            "|year=2023|month=December|day=06}}\n")
            out_gallery += f"{fn1}\n"
        else: continue
        if fn1 in crops:
            demo = PIL.Image.new('RGB', (3 * demosize + 2 * gap, demosize))
            x, y = crops[fn1][0:2]
            scale = i2.width / i1.width
            crop2 = i2.crop((x*scale-demosize/2, y*scale-demosize/2,
                             x*scale+demosize/2, y*scale+demosize/2))
            demo.paste(crop2, (0, 0))
            dr = PIL.ImageDraw.Draw(demo)
            dr.polygon([(demosize/2-(demosize/2 * scale),
                         demosize/2-(demosize/2 * scale)),
                        (demosize/2+(demosize/2 * scale),
                         demosize/2-(demosize/2 * scale)),
                        (demosize/2+(demosize/2 * scale),
                         demosize/2+(demosize/2 * scale)),
                        (demosize/2-(demosize/2 * scale),
                         demosize/2+(demosize/2 * scale))])
            scrop2 = i2.resize((demosize, demosize),
                               box=((x-demosize/2)*scale, (y-demosize/2)*scale,
                                    (x+demosize/2)*scale, (y+demosize/2)*scale))
            demo.paste(scrop2, (demosize+gap, 0))
            crop1 = i1.crop((x-demosize/2, y-demosize/2,
                             x+demosize/2, y+demosize/2))
            demo.paste(crop1, (2*demosize+2*gap, 0))
            demo.save(f"demo{ctr:03}.tiff", "TIFF")
            out_table += "||[{{canonicalurl:File:Medium Geograph screenshot comparison.tiff|page=" f"{ctr}" "}}]"
            ctr += 1
        out_table += "\n"
out_table += "|}\n"
out_gallery += "</gallery>\n"

print(out_list)
print(out_table)
print(out_deltags)
print(out_gallery)
