import PIL.Image
from pathlib import Path

def f_size(f):
    return PIL.Image.open(Path("files") / f,
                          formats=["TIFF", "PNG", "JPEG"]).size

out_table = "{|class=wikitable\n!Geograph download||Screenshot\n"
out_list = ""
out_deltags = ""
out_gallery = "<gallery>\n"

with open("filelist") as fp:
    for line in fp:
        fn1, fn2 = line.rstrip().split('|')
        fs1, fs2 = map(f_size, [fn1, fn2])
        if fs1 < fs2:
            out_table += ("|-\n"
                          f"|({fs2[0]} × {fs2[1]}) [[:File:{fn2}]]|"
                          f"|({fs1[0]} × {fs1[1]}) [[:File:{fn1}]]\n")
            out_list += f"* [[:File:{fn1}]]\n"
            out_deltags += f"File:{fn1}\n"
            out_deltags += ("{{delete|reason=[[COM:Redundant|]] with "
                            f"[[:File:{fn2}]]"
                            "|subpage=Smaller screenshots of Geograph images"
                            "|year=2023|month=November|day=05}}\n")
            out_gallery += f"{fn1}\n"
out_table += "|}\n"
out_gallery += "</gallery>\n"

print(out_list)
print(out_table)
print(out_deltags)
print(out_gallery)
